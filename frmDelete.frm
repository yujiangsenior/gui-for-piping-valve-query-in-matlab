VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "阀门查询"
   ClientHeight    =   5625
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   8655
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   12
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   5625
   ScaleWidth      =   8655
   StartUpPosition =   3  '窗口缺省
   Begin VB.CheckBox Check2 
      Caption         =   "气动"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2520
      TabIndex        =   24
      Top             =   840
      Width           =   1095
   End
   Begin VB.CheckBox Check1 
      Caption         =   "电动"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1320
      TabIndex        =   23
      Top             =   840
      Width           =   975
   End
   Begin VB.CommandButton Command4 
      Caption         =   "不常用阀门"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2400
      TabIndex        =   22
      Top             =   240
      Width           =   2055
   End
   Begin VB.CommandButton Command3 
      Caption         =   "退出"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   6600
      TabIndex        =   20
      Top             =   4320
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "查询"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   4920
      TabIndex        =   15
      Top             =   4320
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "结果"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   4920
      TabIndex        =   6
      Top             =   120
      Width           =   3615
      Begin VB.TextBox Text4 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   1560
         TabIndex        =   10
         Top             =   2280
         Width           =   1695
      End
      Begin VB.TextBox Text3 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   840
         TabIndex        =   9
         Top             =   3240
         Width           =   2655
      End
      Begin VB.TextBox Text2 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1560
         TabIndex        =   8
         Top             =   1320
         Width           =   1695
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   1560
         TabIndex        =   7
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "名称"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   14
         Top             =   3360
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "出口半长/mm"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         TabIndex        =   13
         Top             =   2280
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "进口半长 /mm"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   120
         TabIndex        =   12
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "重量/kg"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   1215
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "常用阀门"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   5
      Top             =   240
      Width           =   1695
   End
   Begin VB.Frame Frame1 
      Caption         =   "参数"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   240
      TabIndex        =   0
      Top             =   1320
      Width           =   4455
      Begin VB.ComboBox Combo5 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   1080
         TabIndex        =   25
         Top             =   1080
         Width           =   3255
      End
      Begin VB.ComboBox Combo4 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   1080
         TabIndex        =   4
         Top             =   3240
         Width           =   1935
      End
      Begin VB.ComboBox Combo3 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   1080
         TabIndex        =   3
         Top             =   2520
         Width           =   2535
      End
      Begin VB.ComboBox Combo2 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   1080
         TabIndex        =   2
         Top             =   1800
         Width           =   3255
      End
      Begin VB.ComboBox Combo1 
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   435
         ItemData        =   "frmDelete.frx":0000
         Left            =   1080
         List            =   "frmDelete.frx":0002
         TabIndex        =   1
         Top             =   360
         Width           =   3255
      End
      Begin VB.Label Label10 
         Caption         =   "名称"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   26
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label Label8 
         Caption         =   "DN"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   19
         Top             =   3360
         Width           =   615
      End
      Begin VB.Label Label7 
         Caption         =   "PN"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   18
         Top             =   2640
         Width           =   615
      End
      Begin VB.Label Label6 
         Caption         =   "代号"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   17
         Top             =   1920
         Width           =   735
      End
      Begin VB.Label Label5 
         Caption         =   "类型"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   16
         Top             =   480
         Width           =   735
      End
   End
   Begin VB.Label Label9 
      Caption         =   "Copyright@中国联合工程公司 www.chinacuc.com"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   10.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   21
      Top             =   5280
      Width           =   5055
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()
    Combo2.Clear
    Combo3.Clear
    Combo4.Clear
    Combo5.Clear
    Combo2.Text = ""
    Combo3.Text = ""
    Combo4.Text = ""
    Combo5.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""
    If Combo1.ListIndex = -1 Then
    Else
    Dim ADOrs1 As New Recordset
    Dim strSQL As String
    Dim arr1() As String
    Dim arr11() As String
    Dim vv As String
    Dim vvv As String
    Dim T(15) As String
    Dim v(15) As String
    
    Dim Num1 As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim showup1 As Integer
    Dim showup2 As Integer
    
    T(0) = "PSGL"
    T(1) = "BUTT"
    T(2) = "DIAP"
    T(3) = "BTRP"
    T(4) = "RELF"
    T(5) = "PLUG"
    T(6) = "EXST"
    T(7) = "BALL"
    T(8) = "CONT"
    T(9) = "GLOB"
    T(10) = "GATE"
    T(11) = "CHEC"
    T(12) = "SPEC"
    T(13) = "ANSI"
    T(14) = "ERRO"
    T(15) = "TRAS"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
'    t0 = Combo1.ListIndex
'    valve = v(t0)   '以上两句应该可以换成valve=combo1.text

    vv = Combo1.Text
    For i = 0 To 15
        If vv = v(i) Then
        j = i
        Exit For
        Else
        End If
    Next
    vvv = T(j)
    If Check1.Value Then
    strSQL = "Select * from valve where TYPE ='" & vvv & "'" & " and COMP_NAM like '%电动%'"
    Else
    strSQL = "Select * from valve where TYPE ='" & vvv & "'" & " and COMP_NAM not like '%电动%'"
    End If
    If Check2.Value Then
    strSQL = strSQL + " and COMP_NAM like '%气动%'"
    Else
    strSQL = strSQL + " and COMP_NAM not like '%气动%'"
    End If
'http://blog.csdn.net/zhoukang0916/article/details/4127266
'https://jingyan.baidu.com/article/e3c78d647b58823c4c85f50f.html

    ADOrs1.ActiveConnection = ADOcn
    ADOrs1.Open strSQL, ADOcn, adOpenKeyset, adLockReadOnly
    
    Num1 = 0
    Do While Not ADOrs1.EOF
        Num1 = Num1 + 1
        ADOrs1.MoveNext
    Loop

    'a = ADOrs1.RecordCount
    Num1 = Num1 + 1
    ReDim arr1(Num1) As String
    ReDim arr11(Num1) As String
    
    If Num1 <= 1 Then
    
    Else
    
    ADOrs1.MoveFirst
    
    arr1(1) = ""
    arr11(1) = ""
    i = 2
    k = 2
    Do Until ADOrs1.EOF = True

        showup1 = 0
        j = 1
        For j = 1 To i - 1
            If arr1(j) = ADOrs1.Fields("DA").Value Then
                showup1 = 1
                Exit For
            Else
            End If
        Next
        
        showup2 = 0
        j = 1
        For j = 1 To k - 1
            If arr11(j) = ADOrs1.Fields("COMP_NAM").Value Then
                showup2 = 1
                Exit For
            Else
            End If
        Next

        If showup1 = 1 Then
            
        Else
            arr1(i) = ADOrs1.Fields("DA").Value
            Combo2.AddItem arr1(i)
            i = i + 1
        End If
        
        If showup2 = 1 Then
            
        Else
            arr11(k) = ADOrs1.Fields("COMP_NAM").Value
            Combo5.AddItem arr11(k)
            k = k + 1
        End If

        ADOrs1.MoveNext
        
    Loop
    End If

    Combo2.Text = Combo2.List(0)
    'Combo5.Text = Combo5.List(0)
    'Combo2.ListIndex = 0
    '如果上句话加上，则点击combo1后combo2出现的同时，combo3也出现数据，即combo2自动执行click
    ADOrs1.Close
    End If
    
End Sub

Private Sub Check2_Click()
    Combo2.Clear
    Combo3.Clear
    Combo4.Clear
    Combo5.Clear
    Combo2.Text = ""
    Combo3.Text = ""
    Combo4.Text = ""
    Combo5.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""
    If Combo1.ListIndex = -1 Then
    Else
    Dim ADOrs1 As New Recordset
    Dim strSQL As String
    Dim arr1() As String
    Dim arr11() As String
    Dim vv As String
    Dim vvv As String
    Dim T(15) As String
    Dim v(15) As String
    
    Dim Num1 As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim showup1 As Integer
    Dim showup2 As Integer
    
    T(0) = "PSGL"
    T(1) = "BUTT"
    T(2) = "DIAP"
    T(3) = "BTRP"
    T(4) = "RELF"
    T(5) = "PLUG"
    T(6) = "EXST"
    T(7) = "BALL"
    T(8) = "CONT"
    T(9) = "GLOB"
    T(10) = "GATE"
    T(11) = "CHEC"
    T(12) = "SPEC"
    T(13) = "ANSI"
    T(14) = "ERRO"
    T(15) = "TRAS"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
'    t0 = Combo1.ListIndex
'    valve = v(t0)   '以上两句应该可以换成valve=combo1.text

    vv = Combo1.Text
    For i = 0 To 15
        If vv = v(i) Then
        j = i
        Exit For
        Else
        End If
    Next
    vvv = T(j)
    If Check1.Value Then
    strSQL = "Select * from valve where TYPE ='" & vvv & "'" & " and COMP_NAM like '%电动%'"
    Else
    strSQL = "Select * from valve where TYPE ='" & vvv & "'" & " and COMP_NAM not like '%电动%'"
    End If
    If Check2.Value Then
    strSQL = strSQL + " and COMP_NAM like '%气动%'"
    Else
    strSQL = strSQL + " and COMP_NAM not like '%气动%'"
    End If
'http://blog.csdn.net/zhoukang0916/article/details/4127266
'https://jingyan.baidu.com/article/e3c78d647b58823c4c85f50f.html

    ADOrs1.ActiveConnection = ADOcn
    ADOrs1.Open strSQL, ADOcn, adOpenKeyset, adLockReadOnly
    
    Num1 = 0
    Do While Not ADOrs1.EOF
        Num1 = Num1 + 1
        ADOrs1.MoveNext
    Loop

    'a = ADOrs1.RecordCount
    Num1 = Num1 + 1
    ReDim arr1(Num1) As String
    ReDim arr11(Num1) As String
    
    If Num1 <= 1 Then
    
    Else
    
    ADOrs1.MoveFirst
    
    arr1(1) = ""
    arr11(1) = ""
    i = 2
    k = 2
    Do Until ADOrs1.EOF = True

        showup1 = 0
        j = 1
        For j = 1 To i - 1
            If arr1(j) = ADOrs1.Fields("DA").Value Then
                showup1 = 1
                Exit For
            Else
            End If
        Next
        
        showup2 = 0
        j = 1
        For j = 1 To k - 1
            If arr11(j) = ADOrs1.Fields("COMP_NAM").Value Then
                showup2 = 1
                Exit For
            Else
            End If
        Next

        If showup1 = 1 Then
            
        Else
            arr1(i) = ADOrs1.Fields("DA").Value
            Combo2.AddItem arr1(i)
            i = i + 1
        End If
        
        If showup2 = 1 Then
            
        Else
            arr11(k) = ADOrs1.Fields("COMP_NAM").Value
            Combo5.AddItem arr11(k)
            k = k + 1
        End If

        ADOrs1.MoveNext
        
    Loop
    End If

    Combo2.Text = Combo2.List(0)
    'Combo5.Text = Combo5.List(0)
    'Combo2.ListIndex = 0
    '如果上句话加上，则点击combo1后combo2出现的同时，combo3也出现数据，即combo2自动执行click
    ADOrs1.Close
    End If
End Sub

Private Sub Combo1_Click() '主要功能是将某阀门类别下的所有阀门名称放入combo2

    Combo2.Clear
    Combo3.Clear
    Combo4.Clear
    Combo5.Clear
    Combo2.Text = ""
    Combo3.Text = ""
    Combo4.Text = ""
    Combo5.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""
    Dim ADOrs1 As New Recordset
    Dim strSQL As String
    Dim arr1() As String
    Dim arr11() As String
    Dim vv As String
    Dim vvv As String
    Dim T(15) As String
    Dim v(15) As String
    
    Dim Num1 As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim showup1 As Integer
    Dim showup2 As Integer
    
    T(0) = "PSGL"
    T(1) = "BUTT"
    T(2) = "DIAP"
    T(3) = "BTRP"
    T(4) = "RELF"
    T(5) = "PLUG"
    T(6) = "EXST"
    T(7) = "BALL"
    T(8) = "CONT"
    T(9) = "GLOB"
    T(10) = "GATE"
    T(11) = "CHEC"
    T(12) = "SPEC"
    T(13) = "ANSI"
    T(14) = "ERRO"
    T(15) = "TRAS"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
'    t0 = Combo1.ListIndex
'    valve = v(t0)   '以上两句应该可以换成valve=combo1.text

    vv = Combo1.Text
    For i = 0 To 15
        If vv = v(i) Then
        j = i
        Exit For
        Else
        End If
    Next
    vvv = T(j)
    If Check1.Value Then
    strSQL = "Select * from valve where TYPE ='" & vvv & "'" & " and COMP_NAM like '%电动%'"
    Else
    strSQL = "Select * from valve where TYPE ='" & vvv & "'" & " and COMP_NAM not like '%电动%'"
    End If
    If Check2.Value Then
    strSQL = strSQL + " and COMP_NAM like '%气动%'"
    Else
    strSQL = strSQL + " and COMP_NAM not like '%气动%'"
    End If
'http://blog.csdn.net/zhoukang0916/article/details/4127266
'https://jingyan.baidu.com/article/e3c78d647b58823c4c85f50f.html

    ADOrs1.ActiveConnection = ADOcn
    ADOrs1.Open strSQL, ADOcn, adOpenKeyset, adLockReadOnly
    
    Num1 = 0
    Do While Not ADOrs1.EOF
        Num1 = Num1 + 1
        ADOrs1.MoveNext
    Loop

    'a = ADOrs1.RecordCount
    Num1 = Num1 + 1
    ReDim arr1(Num1) As String
    ReDim arr11(Num1) As String
    
If Num1 <= 1 Then
    
Else
    
    ADOrs1.MoveFirst
    
    arr1(1) = ""
    arr11(1) = ""
    i = 2
    k = 2
    Do Until ADOrs1.EOF = True

        showup1 = 0
        j = 1
        For j = 1 To i - 1
            If arr1(j) = ADOrs1.Fields("DA").Value Then
                showup1 = 1
                Exit For
            Else
            End If
        Next
        
        showup2 = 0
        j = 1
        For j = 1 To k - 1
            If arr11(j) = ADOrs1.Fields("COMP_NAM").Value Then
                showup2 = 1
                Exit For
            Else
            End If
        Next

        If showup1 = 1 Then
            
        Else
            arr1(i) = ADOrs1.Fields("DA").Value
            Combo2.AddItem arr1(i)
            i = i + 1
        End If
        
        If showup2 = 1 Then
            
        Else
            arr11(k) = ADOrs1.Fields("COMP_NAM").Value
            Combo5.AddItem arr11(k)
            k = k + 1
        End If

        ADOrs1.MoveNext
        
    Loop
End If

    Combo2.Text = Combo2.List(0)
    'Combo5.Text = Combo5.List(0)
    'Combo2.ListIndex = 0
    '如果上句话加上，则点击combo1后combo2出现的同时，combo3也出现数据，即combo2自动执行click
    ADOrs1.Close
    
End Sub

Private Sub Combo2_Click() '主要功能是将所有查出代号带PN放入combo3
'    List1.Clear
    Combo3.Clear
    Combo4.Clear
'    If Combo5.ListIndex = -1 Then
'    Combo5.Clear
'    Combo5.Text = ""
'    Else
'    End If
    Combo3.Text = ""
    Combo4.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""
    Dim ADOrs2 As New Recordset
    Dim strSQL As String
    Dim arr2() As String

    Dim vv As String
    Dim vvv As String
    Dim code As String
    Dim T(15) As String
    Dim v(15) As String
    
    Dim Num2 As Integer
    Dim i As Integer
    Dim j As Integer
    Dim showup As Integer

    
    T(0) = "PSGL"
    T(1) = "BUTT"
    T(2) = "DIAP"
    T(3) = "BTRP"
    T(4) = "RELF"
    T(5) = "PLUG"
    T(6) = "EXST"
    T(7) = "BALL"
    T(8) = "CONT"
    T(9) = "GLOB"
    T(10) = "GATE"
    T(11) = "CHEC"
    T(12) = "SPEC"
    T(13) = "ANSI"
    T(14) = "ERRO"
    T(15) = "TRAS"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
    
    vv = Combo1.Text
    For i = 0 To 15
        If vv = v(i) Then
        j = i
        Exit For
        Else
        End If
    Next
    vvv = T(j)
    namee = Combo5.Text
    code = Combo2.Text
    strSQL = "Select * from valve"
    strSQL = strSQL + " where TYPE='" & vvv & "'"
    
    If Combo5.ListIndex = -1 Then
    Else
    strSQL = strSQL + " and COMP_NAM='" & namee & "'"
    End If
    
    strSQL = strSQL + " and DA='" & code & "'"
    
'    strSQL = strSQL & "and PN='" & pn & "'"
    
    
'    Text2.Text = strSQL     '检测用
    
    'strSQL = "Select * from 安全阀 where 名称='/A48Y'"
    ADOrs2.ActiveConnection = ADOcn
    ADOrs2.Open strSQL, ADOcn, adOpenKeyset, adLockReadOnly
    

    Num2 = 0
    Do While Not ADOrs2.EOF
        Num2 = Num2 + 1
        ADOrs2.MoveNext
    Loop
    Num2 = Num2 + 1
    ReDim arr2(Num2) As String


    ADOrs2.MoveFirst
    
    arr2(1) = ""
    i = 2
    Do Until ADOrs2.EOF = True
        showup = 0
        j = 1
        For j = 1 To i - 1
            If arr2(j) = ADOrs2.Fields("PN").Value Then
                showup = 1 '代表前面出现过
                Exit For
            Else
            End If
        Next
'        List1.AddItem ADOrs2.Fields("CATNO").Value
        If showup = 1 Then
            
        Else
            arr2(i) = ADOrs2.Fields("PN").Value
            Combo3.AddItem arr2(i)
            i = i + 1
        End If
        'Combo3.ListIndex = 0
        ADOrs2.MoveNext
        
    Loop
    

    Combo3.Text = Combo3.List(0)

    ADOrs2.Close
    
End Sub

Private Sub Combo3_Click() '主要功能是将所有查出DN放入combo4
'    List1.Clear
    Combo4.Clear
    Combo4.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""

    Dim ADOrs3 As New Recordset
    Dim strSQL As String
    
    Dim vv As String
    Dim vvv As String
    Dim namee As String
    Dim code As String
    Dim pn As String
    Dim T(15) As String
    Dim v(15) As String
    
    Dim i As Integer
    Dim j As Integer
    
    T(0) = "PSGL"
    T(1) = "BUTT"
    T(2) = "DIAP"
    T(3) = "BTRP"
    T(4) = "RELF"
    T(5) = "PLUG"
    T(6) = "EXST"
    T(7) = "BALL"
    T(8) = "CONT"
    T(9) = "GLOB"
    T(10) = "GATE"
    T(11) = "CHEC"
    T(12) = "SPEC"
    T(13) = "ANSI"
    T(14) = "ERRO"
    T(15) = "TRAS"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
    
    vv = Combo1.Text
    For i = 0 To 15
        If vv = v(i) Then
        j = i
        Exit For
        Else
        End If
    Next
    vvv = T(j)
    namee = Combo5.Text
    code = Combo2.Text
    pn = Combo3.Text
    'pn = Combo3.Text '此处不知道pn作为字符变量读取压力等级时会不会出错
    strSQL = "Select * from valve"
    strSQL = strSQL + " where TYPE='" & vvv & "'"
    
    If Combo5.ListIndex = -1 Then
    Else
    strSQL = strSQL + " and COMP_NAM='" & namee & "'"
    End If
    
    strSQL = strSQL + " and DA='" & code & "'"
    strSQL = strSQL + " and PN='" & pn & "'"
    
    ADOrs3.ActiveConnection = ADOcn
    ADOrs3.Open strSQL, ADOcn, adOpenKeyset, adLockReadOnly


    Do Until ADOrs3.EOF = True
'        List1.AddItem ADOrs3.Fields("DN").Value
        Combo4.AddItem ADOrs3.Fields("COMP_SPE").Value '此处可能有问题，因为涉及不同厂家同一款阀门
        
        ADOrs3.MoveNext
        
    Loop
    
    Combo4.Text = Combo4.List(0)
    'Combo4.ListIndex = 0
    
    ADOrs3.Close
    
End Sub

Private Sub Combo4_Click() '主要功能是查出所需信息并显示在文本框
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""
    
    
End Sub

Private Sub Combo5_Click()
    Combo2.Clear
    Combo3.Clear
    Combo4.Clear
    Combo2.Text = ""
    Combo3.Text = ""
    Combo4.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""
    Dim ADOrs5 As New Recordset
    Dim strSQL As String
    
    Dim vv As String
    Dim vvv As String

    Dim T(15) As String
    Dim v(15) As String
    
    Dim Num5 As Integer
    Dim i As Integer
    Dim j As Integer
    Dim showup As Integer
    
    T(0) = "PSGL"
    T(1) = "BUTT"
    T(2) = "DIAP"
    T(3) = "BTRP"
    T(4) = "RELF"
    T(5) = "PLUG"
    T(6) = "EXST"
    T(7) = "BALL"
    T(8) = "CONT"
    T(9) = "GLOB"
    T(10) = "GATE"
    T(11) = "CHEC"
    T(12) = "SPEC"
    T(13) = "ANSI"
    T(14) = "ERRO"
    T(15) = "TRAS"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
    
    
    vv = Combo1.Text
    For i = 0 To 15
        If vv = v(i) Then
        j = i
        Exit For
        Else
        End If
    Next
    vvv = T(j)
    namee = Combo5.Text
    
    strSQL = "Select * from valve"
    strSQL = strSQL & " where TYPE='" & vvv & "' and COMP_NAM='" & namee & "'"
    
    ADOrs5.ActiveConnection = ADOcn
    ADOrs5.Open strSQL, ADOcn, adOpenKeyset, adLockReadOnly
    

    Num5 = 0
    Do While Not ADOrs5.EOF
        Num5 = Num5 + 1
        ADOrs5.MoveNext
    Loop
    
    Num5 = Num5 + 1
    ReDim arr5(Num5) As String
    
    
    ADOrs5.MoveFirst
    
    arr5(1) = ""
    i = 2
    Do Until ADOrs5.EOF = True
        showup = 0
        j = 1
        For j = 1 To i - 1
            If arr5(j) = ADOrs5.Fields("DA").Value Then
                showup = 1 '代表前面出现过
                Exit For
            Else
            End If
        Next

        If showup = 1 Then
            
        Else
            arr5(i) = ADOrs5.Fields("DA").Value
            Combo2.AddItem arr5(i)
            i = i + 1
        End If

        ADOrs5.MoveNext
    Loop
    
    Combo2.Text = Combo2.List(0)
    
    ADOrs5.Close


End Sub

Private Sub Command1_click() '主要功能是将所有阀门种类放入combo1
    Combo1.Clear
    Combo2.Clear
    Combo3.Clear
    Combo4.Clear
    Combo5.Clear
    Combo1.Text = ""
    Combo2.Text = ""
    Combo3.Text = ""
    Combo4.Text = ""
    Combo5.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""

    Dim v(15) As String
'    v(0) = "安全阀"
'    v(1) = "电动蝶阀"
'    v(2) = "电动减压阀"
'    v(3) = "电动截止阀"
'    v(4) = "电动球阀"
'    v(5) = "电动闸阀"
'    v(6) = "蝶阀"
'    v(7) = "堵阀"
'    v(8) = "过滤阀"
'    v(9) = "角阀"
'    v(10) = "节流阀"
'    v(11) = "减压阀"
'    v(12) = "截止阀"
'    v(13) = "料浆阀"
'    v(14) = "逆止阀"
'    v(15) = "旁路阀"
'    v(16) = "排污阀"
'    v(17) = "切断阀"
'    v(18) = "球阀"
'    v(19) = "三通阀"
'    v(20) = "疏水阀"
'    v(21) = "调节阀"
'    v(22) = "旋塞阀"
'    v(23) = "闸阀"
'    v(24) = "止回阀"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
    For i = 0 To 11
        Combo1.AddItem v(i)
    Next


    Combo1.Text = Combo1.List(0)

    
    
End Sub

Private Sub Command2_Click()

    Dim ADOrs4 As New Recordset
    
    Dim strSQL As String
    Dim vv As String
    Dim vvv As String
    Dim namee As String
    Dim code As String
    Dim pn As String
    Dim T(15) As String
    Dim v(15) As String
    
    Dim i As Integer
    Dim j As Integer
    
    T(0) = "PSGL"
    T(1) = "BUTT"
    T(2) = "DIAP"
    T(3) = "BTRP"
    T(4) = "RELF"
    T(5) = "PLUG"
    T(6) = "EXST"
    T(7) = "BALL"
    T(8) = "CONT"
    T(9) = "GLOB"
    T(10) = "GATE"
    T(11) = "CHEC"
    T(12) = "SPEC"
    T(13) = "ANSI"
    T(14) = "ERRO"
    T(15) = "TRAS"
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
    
    vv = Combo1.Text
    For i = 0 To 15
        If vv = v(i) Then
        j = i
        Exit For
        Else
        End If
    Next
    vvv = T(j)
    namee = Combo5.Text
    code = Combo2.Text
    pn = Combo3.Text

    strSQL = "Select * from valve"
    strSQL = strSQL + " where TYPE='" & vvv & "'"
    
    If Combo5.ListIndex = -1 Then
    Else
    strSQL = strSQL + " and COMP_NAM='" & namee & "'"
    End If
    
    strSQL = strSQL + " and DA='" & code & "'"
    strSQL = strSQL + " and PN='" & pn & "'"
    
    
    
    ADOrs4.ActiveConnection = ADOcn
    ADOrs4.Open strSQL, ADOcn, adOpenKeyset, adLockReadOnly

'    n = Combo4.ListCount
    i = Combo4.ListIndex

    j = 0  '非常重要,i从0开始，因此j也必须从0开始!
    Do Until ADOrs4.EOF = True
        If ADOrs4.Fields("COMP_SPE").Value = Combo4.Text Then
            If j = i Then
                Text1.Text = ADOrs4.Fields("COMP_WGT").Value  '第1个框输出阀门重量
                Text2.Text = ADOrs4.Fields("P1_PAR").Value  '第2个框输出阀门进口半长
                Text4.Text = ADOrs4.Fields("P2_PAR").Value  '第3个框输出阀门出口半长
                If Combo5.ListIndex = -1 Then
                    Text3.Text = ADOrs4.Fields("COMP_NAM").Value
                Else
                
                End If

                Exit Do
            Else
            ADOrs4.MoveNext
            j = j + 1
            End If
            
        Else
            ADOrs4.MoveNext
            j = j + 1
        End If

    Loop

    ADOrs4.Close
End Sub

Private Sub Command3_Click()
    
    'Dim ADOrs5 As New Recordset
    'Dim strSQL As String
    

    'strSQL = "Select * from valve"
    'strSQL = strSQL & " where COMP_NAM='" & namee & "'"
    'strSQL = strSQL & "and CATNO='" & code & "'"
    
    'strSQL = "update valve set TYPE= 'SPEC' where TYPE='GATE' and COMP_NAM= '电动超高真空闸板阀'"
    'strSQL = "update valve set CATNO= 'PZ941H-64' where CATNO= 'PZ941HH-64'"
    'DA like 'BH%'"TYPE='PLUG'
    'and CATNO like 'Z%'"
    'and COMP_NAM='喷水调节阀'"
'    Text3.Text = strSQL '检测用
    'strSQL = "Select * from 安全阀 where 名称='" & "/A48Y" & "'and PN='" & 100 & "'"
    
    'ADOrs5.ActiveConnection = ADOcn
    'ADOcn.Execute strSQL
    
    
    
    Unload Me
End Sub

Private Sub Command4_Click()
    Combo1.Clear
    Combo2.Clear
    Combo3.Clear
    Combo4.Clear
    Combo5.Clear
    Combo1.Text = ""
    Combo2.Text = ""
    Combo3.Text = ""
    Combo4.Text = ""
    Combo5.Text = ""
    Text1.Text = ""
    Text2.Text = ""
    Text3.Text = ""
    Text4.Text = ""

    Dim v(15) As String
    v(0) = "安全阀"
    v(1) = "蝶阀"
    v(2) = "隔膜阀"
    v(3) = "节流阀"
    v(4) = "减压阀"
    v(5) = "截止阀"
    v(6) = "排污阀"
    v(7) = "球阀"
    v(8) = "疏水阀"
    v(9) = "调节阀"
    v(10) = "闸阀"
    v(11) = "止回阀"
    v(12) = "特殊阀门"
    v(13) = "ANSI阀门"
    v(14) = "问题阀门"
    v(15) = "其它阀门"
'    v(0) = "堵阀"
'    v(1) = "过滤阀"
'    v(2) = "角阀"
'    v(3) = "节流阀"
'    v(4) = "料浆阀"
'    v(5) = "逆止阀"
'    v(6) = "旁路阀"
'    v(7) = "排污阀"
'    v(8) = "切断阀"
'    v(9) = "三通阀"
'    v(10) = "旋塞阀"
    For i = 12 To 15
        Combo1.AddItem v(i)
    Next


    Combo1.Text = Combo1.List(0)

End Sub

Private Sub Form_Load()
'    Command1_Click
   
End Sub

